package com.xsor.octane;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.xsor.octane.SQLite.SavedOctane;
import com.xsor.octane.SQLite.SavedOctaneSQLiteHelper;

import java.util.List;

public class FavoritesListAdapter extends RecyclerView.Adapter<FavoritesListAdapter.SavedOctaneViewHolder> {

    private List<SavedOctane> savedOctaneList;
    private Context mContext;

    public FavoritesListAdapter(List<SavedOctane> savedOctaneList, Context context) {
        this.savedOctaneList = savedOctaneList;
        this.mContext = context;
    }


    @Override
    public int getItemCount() {
        return savedOctaneList.size();
    }


    @Override
    public void onBindViewHolder(SavedOctaneViewHolder savedOctaneViewHolder, int i) {
        SavedOctane so = savedOctaneList.get(i);
        savedOctaneViewHolder.lowOctane.setText(so.getLowerOctane());
        savedOctaneViewHolder.lowAmount.setText(so.getLowerAmount());
        savedOctaneViewHolder.highOctane.setText(so.getHigherOctane());
        savedOctaneViewHolder.highAmount.setText(so.getHigherAmount());
        savedOctaneViewHolder.tableHeader.setText(so.getGallons()+" Gallons of Octane " + so.getDesiredOctane());
        savedOctaneViewHolder.resultText.setText("Use " + so.getLowerAmount() + " gallons of " + so.getLowerOctane() +
                " octane and " + so.getHigherAmount() + " gallons of " + so.getHigherOctane() +
                " octane to make " + so.getGallons() + " gallons of " + so.getDesiredOctane() +
                " octane.");
        savedOctaneViewHolder.entryID = so.getID();
    }

    @Override
    public SavedOctaneViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.saved_octane_card_layout, viewGroup, false);

        return new SavedOctaneViewHolder(itemView);
    }



    public class SavedOctaneViewHolder extends RecyclerView.ViewHolder {

        protected long     entryID;
        protected TextView lowOctane;
        protected TextView lowAmount;
        protected TextView highOctane;
        protected TextView highAmount;
        protected TextView tableHeader;
        protected TextView resultText;
        protected Button   deleteButton;

        public SavedOctaneViewHolder(View v) {
            super(v);

            lowOctane =    (TextView) v.findViewById(R.id.resultLowOctane);
            lowAmount =    (TextView) v.findViewById(R.id.resultLowAmount);
            highOctane =   (TextView) v.findViewById(R.id.resultHighOctane);
            highAmount =   (TextView) v.findViewById(R.id.resultHighAmount);
            tableHeader =  (TextView) v.findViewById(R.id.tableHeader);
            resultText =   (TextView) v.findViewById(R.id.resultText);
            deleteButton =   (Button) v.findViewById(R.id.delete);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SavedOctaneSQLiteHelper db = new SavedOctaneSQLiteHelper(mContext);
                    db.deleteOctaneEntry(entryID);
                    removeAt(getLayoutPosition());
                    Log.i("Deleted from: ", "List: " + getLayoutPosition() + " DB: " + entryID);
                }
            });
        }
    }

    public void removeAt(int position) {
        savedOctaneList.remove(position);
	    notifyItemRemoved(position);
	    notifyItemRangeChanged(position, savedOctaneList.size());
    }
}
