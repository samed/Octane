package com.xsor.octane;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.xsor.octane.SQLite.SavedOctane;
import com.xsor.octane.SQLite.SavedOctaneSQLiteHelper;

import java.util.List;

public class FavoritesList extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites_list);

        // Enable back (button) from ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	    AdView mAdView = (AdView) findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    mAdView.loadAd(adRequest);


	    RecyclerView recList = (RecyclerView) findViewById(R.id.cardList);
	    // recList.setHasFixedSize(true);
	    LinearLayoutManager llm = new LinearLayoutManager(this);
	    llm.setOrientation(LinearLayoutManager.VERTICAL);
	    recList.setLayoutManager(llm);

	    SavedOctaneSQLiteHelper db = new SavedOctaneSQLiteHelper(this);
	    db.close();
	    // Get All Entries
	    List<SavedOctane> favoritesList = db.getAllOctaneEntries();

	    // If no entries, display alert.
	    if(favoritesList.size() == 0) {
		    AlertDialog.Builder noFavoritesBuilder = new AlertDialog.Builder(this);
		    noFavoritesBuilder.setCancelable(false);
		    noFavoritesBuilder.setTitle("No Favorites Found");
		    noFavoritesBuilder.setMessage(getString(R.string.noFavoritesFound));
		    noFavoritesBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog, int which) {
				    finish();
			    }
		    });

		    AlertDialog noFavoritesAlert = noFavoritesBuilder.create();
		    noFavoritesAlert.show();

	    } else {
		    FavoritesListAdapter favoritesAdapter = new FavoritesListAdapter(favoritesList, this);
		    recList.setAdapter(favoritesAdapter);
	    }

    }

	@Override
	public void onStart() {
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);

	}

	@Override
	public void onStop() {
		super.onStop();
		//Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

}
