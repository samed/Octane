package com.xsor.octane;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.joanzapata.android.iconify.IconDrawable;
import com.joanzapata.android.iconify.Iconify.IconValue;
import com.xsor.octane.SQLite.SavedOctane;
import com.xsor.octane.SQLite.SavedOctaneSQLiteHelper;

import java.lang.reflect.Field;
import java.math.BigDecimal;


// Admob Ads
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class Octane extends ActionBarActivity {

    public static final String PREFS_NAME = "OctanePreferences" ;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    boolean showDisclaimerMessage;

    EditText desiredOctane, lowerOctane, higherOctane, desiredGallons;
    TextView result, resultLowOctane, resultHighOctane, resultLowAmount, resultHighAmount;
    Button saveOctaneEntry;

    int desiredOctaneInt, lowerOctaneInt, higherOctaneInt, desiredGallonsInt;
    float ratioHigher, ratioLower, lowerGallons, higherGallons;

    CheckBox showAgain;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_octane);

        //Get a Tracker (should auto-report)
        ((OctaneApplication) getApplication()).getTracker(OctaneApplication.TrackerName.APP_TRACKER);

        // Show disclaimer if necessary.
        settings = getSharedPreferences(PREFS_NAME, 0);
        showDisclaimer(false);

        // Force overflow menu
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");

            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            // presumably, not relevant
        }

        // Initialize Variables
        desiredOctane       = (EditText) findViewById(R.id.desiredOctane);
        lowerOctane         = (EditText) findViewById(R.id.lowerOctane);
        higherOctane        = (EditText) findViewById(R.id.higherOctane);
        desiredGallons      = (EditText) findViewById(R.id.desiredGallons);
        result              = (TextView) findViewById(R.id.result);
        resultLowOctane     = (TextView) findViewById(R.id.resultLowOctane);
        resultHighOctane    = (TextView) findViewById(R.id.resultHighOctane);
        resultLowAmount     = (TextView) findViewById(R.id.resultLowAmount);
        resultHighAmount    = (TextView) findViewById(R.id.resultHighAmount);
        saveOctaneEntry     = (Button)   findViewById(R.id.saveOctaneEntry);


	    AdView mAdView = (AdView) findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    mAdView.loadAd(adRequest);

    }

    protected void showDisclaimer(boolean forceShow) {

        AlertDialog.Builder disclaimerAlertBuilder = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View checkboxLayout = adbInflater.inflate(R.layout.show_disclaimer_checkbox, null);
        showAgain = (CheckBox)checkboxLayout.findViewById(R.id.showDisclaimerCheckBox);
        showDisclaimerMessage = settings.getBoolean("showAgain", true);
        showAgain.setChecked(showDisclaimerMessage);
        disclaimerAlertBuilder.setView(checkboxLayout);
        disclaimerAlertBuilder.setCancelable(false);
        disclaimerAlertBuilder.setTitle("Disclaimer");
        disclaimerAlertBuilder.setMessage(getString(R.string.disclaimer));
        disclaimerAlertBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                editor = settings.edit();
                editor.putBoolean("showAgain", showAgain.isChecked());
                editor.apply();
            }
        });

        disclaimerAlertBuilder.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                editor = settings.edit();
                editor.putBoolean("showAgain", true);
                editor.apply();
                System.exit(0);
            }
        });
        AlertDialog disclaimerAlert = disclaimerAlertBuilder.create();


        if (showDisclaimerMessage || forceShow) {
            disclaimerAlert.show();
        }
    }

    // OnClickListener method for calculate button, defined in activity_octane
    public void calculateDesiredGallons(View v) {
        if(isEmpty(desiredOctane)) {
            Cheers("Desired Octane cannot be empty!");
            //desiredOctane.setError("Please enter a number!");
        } else if(isEmpty(lowerOctane)) {
            Cheers("Lower Octane cannot be empty!");
            //lowerOctane.setError("Please enter a number!");
        } else if(isEmpty(higherOctane)) {
            Cheers("Higher octane cannot be empty!");
            //higherOctane.setError("Please enter a number!");
        } else if(isEmpty(desiredGallons)) {
            Cheers("Desired gallons cannot be empty!");
            //desiredGallons.setError("Please enter a number!");
        } else {
            // Do the calculation and display result.
            desiredOctaneInt = Integer.parseInt(desiredOctane.getText().toString());
            lowerOctaneInt = Integer.parseInt(lowerOctane.getText().toString());
            higherOctaneInt = Integer.parseInt(higherOctane.getText().toString());
            desiredGallonsInt = Integer.parseInt(desiredGallons.getText().toString());

            if(lowerOctaneInt > higherOctaneInt || lowerOctaneInt == higherOctaneInt) {
                Cheers("Higher Octane must be higher than Lower Octane!");
            } else if (desiredOctaneInt > higherOctaneInt || desiredOctaneInt < lowerOctaneInt || desiredOctaneInt == lowerOctaneInt || desiredOctaneInt == higherOctaneInt) {
                Cheers("Desired octane must be between lower and higher octane!");
            } else {
                ratioHigher = (float) (desiredOctaneInt - lowerOctaneInt) / (higherOctaneInt - lowerOctaneInt);

                ratioLower = 1 - ratioHigher;

                lowerGallons = round(ratioLower * desiredGallonsInt, 2);
                higherGallons = round(ratioHigher * desiredGallonsInt, 2);

                result.setText("Use " + Float.toString(lowerGallons) + " gallons of " + lowerOctane.getText() +
                        " octane and " + Float.toString(higherGallons) + " gallons of " + higherOctane.getText() +
                        " octane to make " + desiredGallons.getText() + " gallons of " + desiredOctane.getText() +
                        " octane.");

                resultLowOctane.setText(lowerOctane.getText());
                resultHighOctane.setText(higherOctane.getText());
                resultLowAmount.setText(Float.toString(lowerGallons));
                resultHighAmount.setText(Float.toString(higherGallons));
                saveOctaneEntry.setVisibility(View.VISIBLE);
            }
        }

    }

    // OnClickListener method for "Save to Favorites" button, defined in activity_octane.xml
    public void addToFavorites(View v) {

        SavedOctaneSQLiteHelper db = new SavedOctaneSQLiteHelper(this);

        db.addOctaneEntry(new SavedOctane(resultLowOctane.getText().toString(), resultLowAmount.getText().toString(),
		        resultHighOctane.getText().toString(), resultHighAmount.getText().toString(),
		        desiredOctane.getText().toString(), desiredGallons.getText().toString()));

        Cheers("Saved to favorites!");

	    db.close();
    }

    public void resetInputs(View v) {
    // OnClickListener method for reset button, defined in activity_octane.xml
        desiredOctane.setText("");
        lowerOctane.setText("");
        higherOctane.setText("");
        desiredGallons.setText("");
        resultLowOctane.setText("");
        resultHighOctane.setText("");
        resultLowAmount.setText("");
        resultHighAmount.setText("");
        result.setText("");
        saveOctaneEntry.setVisibility(View.INVISIBLE);
    }

    public void Cheers(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private boolean isEmpty(EditText et) {
        return et.getText().toString().trim().length() == 0;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

	@Override
	public void onStart() {
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		//Stop the analytics tracking
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_octane_input, menu);

        // Set an icon in the ActionBar
        menu.findItem(R.id.show_saved_octanes).setIcon(
                new IconDrawable(this, IconValue.fa_star)
                        .colorRes(R.color.white)
                        .actionBarSize());

        // Set an icon in the ActionBar
        menu.findItem(R.id.show_disclaimer).setIcon(
                new IconDrawable(this, IconValue.fa_warning)
                        .colorRes(R.color.white)
                        .actionBarSize());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id) {
            case R.id.show_disclaimer:
                showDisclaimer(true);
            break;

            case R.id.show_saved_octanes:
                // Start activity to show saved list.
                Intent intent = new Intent(this, FavoritesList.class);
                startActivity(intent);
            break;
        }

        return super.onOptionsItemSelected(item);
    }
}
