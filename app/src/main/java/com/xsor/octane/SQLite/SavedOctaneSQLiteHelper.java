package com.xsor.octane.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

public class SavedOctaneSQLiteHelper extends SQLiteOpenHelper {

    // Database Name
    private static final String DATABASE_NAME = "OctanePlus";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Saved Octanes Table
    public static final String TABLE_SAVED_OCTANES      = "SavedOctanes";

    // Column Names
    public static final String COLUMN_ID                = "ID";
    public static final String COLUMN_LOWER_OCTANE      = "LowerOctane";
    public static final String COLUMN_LOWER_AMOUNT      = "LowerAmount";
    public static final String COLUMN_HIGHER_OCTANE     = "HigherOctane";
    public static final String COLUMN_HIGHER_AMOUNT     = "HigherAmount";
    public static final String COLUMN_DESIRED_OCTANE    = "DesiredOctane";
    public static final String COLUMN_GALLONS           = "Gallons";

    // Constant defines
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    // Query to create the Saved Octanes table.
    private static final String DATABASE_CREATE =
            "CREATE TABLE " + TABLE_SAVED_OCTANES + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY," +
                    COLUMN_LOWER_OCTANE + TEXT_TYPE + COMMA_SEP +
                    COLUMN_LOWER_AMOUNT + TEXT_TYPE + COMMA_SEP +
                    COLUMN_HIGHER_OCTANE + TEXT_TYPE + COMMA_SEP +
                    COLUMN_HIGHER_AMOUNT + TEXT_TYPE + COMMA_SEP +
                    COLUMN_DESIRED_OCTANE + TEXT_TYPE + COMMA_SEP +
                    COLUMN_GALLONS + TEXT_TYPE + " )";

    /*
    private String[] allColumns = { COLUMN_ID, COLUMN_LOWER_OCTANE,
                COLUMN_HIGHER_OCTANE,COLUMN_LOWER_AMOUNT,
                COLUMN_HIGHER_AMOUNT, COLUMN_DESIRED_OCTANE, COLUMN_GALLONS };
    */

    public SavedOctaneSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(SavedOctaneSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED_OCTANES);
        onCreate(db);
    }

    /**
     * CRUD operations (create "add", read "get", update, delete)
     */

    public void addOctaneEntry(SavedOctane savedOctaneEntry) {
        /// Log adding book
        Log.d("addOctaneEntry",savedOctaneEntry.toString());

        // 1. Get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. Create ContentValues to add key column/value
        ContentValues values = new ContentValues();
        values.put(COLUMN_LOWER_OCTANE,     savedOctaneEntry.getLowerOctane());
        values.put(COLUMN_LOWER_AMOUNT,     savedOctaneEntry.getLowerAmount());
        values.put(COLUMN_HIGHER_OCTANE,    savedOctaneEntry.getHigherOctane());
        values.put(COLUMN_HIGHER_AMOUNT,    savedOctaneEntry.getHigherAmount());
        values.put(COLUMN_DESIRED_OCTANE,   savedOctaneEntry.getDesiredOctane());
        values.put(COLUMN_GALLONS,          savedOctaneEntry.getGallons());

        // 3. Insert
        db.insert(TABLE_SAVED_OCTANES, // Table
                    null, // nullColumnHack
                    values);

        // 4. Close
        db.close();

    }

    /* Get single entry
    public SavedOctane getOctaneEntry(long id) {
        // 1. Get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. Build Query
        Cursor cursor =
             db.query(TABLE_SAVED_OCTANES, // a. table.
                allColumns, // b. column names
                " ID = ?", // c. selections
                new String[] { String.valueOf(id) }, // d. selection args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        // 3. Get to first result.
            cursor.moveToFirst();

        // 4. Build savedOctane object
        SavedOctane savedOctane = new SavedOctane();
        savedOctane.setID(Long.parseLong(cursor.getString(0)));
        savedOctane.setLowerOctane(cursor.getString(1));
        savedOctane.setLowerAmount(cursor.getString(2));
        savedOctane.setHigherOctane(cursor.getString(3));
        savedOctane.setHigherAmount(cursor.getString(4));
        savedOctane.setDesiredOctane(cursor.getString(5));
        savedOctane.setGallons(cursor.getString(6));

        // Log it
        Log.d("getOctaneEntry("+id+")", savedOctane.toString());

        // 5. Return savedOctane
        return savedOctane;
    }
    */

    public List<SavedOctane> getAllOctaneEntries() {
        List<SavedOctane> savedOctanes = new LinkedList<SavedOctane>();

        // 1. Build the query
        String query = "SELECT * FROM " + TABLE_SAVED_OCTANES;

        // 2. Get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query,null);

        // 3. Go over each row, build savedOctane entry and add it ot the list
        SavedOctane savedOctane;

        if(cursor.moveToFirst()) {
            do {
                savedOctane = new SavedOctane();
                savedOctane.setID(Long.parseLong(cursor.getString(0)));
                savedOctane.setLowerOctane(cursor.getString(1));
                savedOctane.setLowerAmount(cursor.getString(2));
                savedOctane.setHigherOctane(cursor.getString(3));
                savedOctane.setHigherAmount(cursor.getString(4));
                savedOctane.setDesiredOctane(cursor.getString(5));
                savedOctane.setGallons(cursor.getString(6));

                // Add savedOctane to savedOctanes list
                savedOctanes.add(savedOctane);
            } while (cursor.moveToNext());
        }

        Log.d("getAllOctaneEntries()",savedOctanes.toString());

        return savedOctanes;
    }

    /* Update single entry
    public int updateOctaneEntry(SavedOctane octaneEntry){

        // 1. Get reference to writable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. Crate ContentValues to add key/column values
        ContentValues values = new ContentValues();
        values.put(COLUMN_LOWER_OCTANE,octaneEntry.getLowerOctane());
        values.put(COLUMN_LOWER_AMOUNT,octaneEntry.getLowerAmount());
        values.put(COLUMN_HIGHER_OCTANE,octaneEntry.getHigherOctane());
        values.put(COLUMN_HIGHER_AMOUNT,octaneEntry.getHigherAmount());
        values.put(COLUMN_DESIRED_OCTANE,octaneEntry.getDesiredOctane());
        values.put(COLUMN_GALLONS,octaneEntry.getGallons());


        // 3. Updating the row
        int i = db.update(TABLE_SAVED_OCTANES, // Table
                        values, // column/values
                        COLUMN_ID+" = ?", // selections
                        new String[] { String.valueOf(octaneEntry.getID()) }); // selection

        // 4. Close db
        db.close();

        Log.d("updateOctaneEntry()", octaneEntry.toString());

        return i;
    }
    */

    public void deleteOctaneEntry(long id) {

        // 1. Get reference to writable DB
        SQLiteDatabase db =  this.getWritableDatabase();

        // 2. Delete
        db.delete(TABLE_SAVED_OCTANES, // table name
                COLUMN_ID + " = ?",    // selections
                new String[] { String.valueOf(id) } ); // selection

        // 3. Close
        db.close();

        Log.d("deleteOctaneEntry()", String.valueOf(id));
    }
}
