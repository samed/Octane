package com.xsor.octane.SQLite;

/**
 * Created by Samed on 10/30/2014.
 * SavedOctane Object.
 */
public class SavedOctane {

    private long ID;
    private String LowerOctane;
    private String HigherOctane;
    private String LowerAmount;
    private String HigherAmount;
    private String DesiredOctane;
    private String Gallons;

    public SavedOctane() {}

    public SavedOctane(String LowerOctane, String LowerAmount,
                       String HigherOctane, String HigherAmount,
                       String DesiredOctane, String Gallons) {
        super();
        this.LowerOctane = LowerOctane;
        this.HigherOctane = HigherOctane;
        this.LowerAmount = LowerAmount;
        this.HigherAmount = HigherAmount;
        this.DesiredOctane = DesiredOctane;
        this.Gallons = Gallons;

    }

    // Setters
    public void setID(long id) {
        this.ID = id;
    }

    public void setLowerOctane(String lowerOctane) {
        this.LowerOctane = lowerOctane;
    }

    public void setLowerAmount(String lowerAmount) {
        this.LowerAmount = lowerAmount;
    }

    public void setHigherOctane(String higherOctane) {
        this.HigherOctane = higherOctane;
    }

    public void setHigherAmount(String higherAmount) {
        this.HigherAmount = higherAmount;
    }

    public void setDesiredOctane(String desiredOctane) { this.DesiredOctane = desiredOctane;}

    public void setGallons(String gallons) {
        this.Gallons = gallons;
    }

    // Getters

    public long getID() {
        return this.ID;
    }

    public String getLowerOctane() {
        return this.LowerOctane;
    }

    public String getLowerAmount() {
        return this.LowerAmount;
    }

    public String getHigherOctane() {
        return this.HigherOctane;
    }

    public String getHigherAmount() {
        return this.HigherAmount;
    }

    public String getDesiredOctane() {
       return this.DesiredOctane;
   }

    public String getGallons() {
        return this.Gallons;
    }

    @Override
    public String toString() {
        return "SavedOctane [LowerOctane:" + LowerOctane +
                "LowerAmount:" + LowerAmount +
                "HigherOctane:" + HigherOctane +
                "HigherAmount:" + HigherAmount +
                "DesiredOctane:" + DesiredOctane +
                "Gallons:" + Gallons + "]";
    }

}
